#ifndef TGKERNEL_TIMER_H
#define TGKERNEL_TIMER_H

#include <chrono>
#include <iostream>


class Timer {

public:

    void writeOutTime(std::chrono::time_point<std::chrono::system_clock> t1,
                      std::chrono::time_point<std::chrono::system_clock> t2, std::string fname);
    void startTimer();
    void stopTimer(std::string fname);

private:

    std::chrono::time_point<std::chrono::system_clock> t1;
    std::chrono::time_point<std::chrono::system_clock> t2;
};


#endif //TGKERNEL_TIMER_H
