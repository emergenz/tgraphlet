#include <iostream>
#include "util/DataSetLoader.h"
#include "util/TemporalGraphs.h"
#include "TemporalGraphletKernel.h"
#include "TemporalWedgeKernel.h"

using namespace std;

/**
 * loads dataset and constructs temporal graph streams
 */
string constructTGStreams(string const dataset, TemporalGraphStreams &tgstreams, unsigned int const min_nodes, unsigned int const min_edges) {
    DataManager dm;
	string datasetname = dm.loadDataSet(dataset);

    for (unsigned int graph_num = 0; graph_num < dm.num_graphs; graph_num++) {

        std::vector<std::vector<unsigned int>> graph_node_labels;
		std::vector<std::vector<unsigned int>> graph_edge_labels;
        std::vector<std::pair<unsigned int, unsigned int>> graph_edges;
		Label graph_label;

        dm.getGraph(graph_num, graph_node_labels, graph_edge_labels, graph_edges, graph_label);

        if (graph_node_labels.empty() || graph_edges.empty()) continue;

        TemporalGraphStream tgs;

		for (unsigned int i = 0; i < graph_edge_labels.size() && i < graph_edges.size(); ++i) {
			auto e = graph_edges[i];

			for (unsigned long label : graph_edge_labels[i]) {

				TemporalEdge tedge;
				tedge.u_id = e.first;
				tedge.v_id = e.second;
				tedge.t = label;

				tgs.edges.push_back(tedge);
			}
		}

		tgs.sort_edges();
        for (size_t i = 0; i < graph_node_labels.size(); ++i) {
            TimedLabelVec tl;
            for (size_t j = 0; j < graph_node_labels.at(i).size() - 1; j += 2) {
                tl.push_back({graph_node_labels.at(i).at(j), graph_node_labels.at(i).at(j + 1)});
            }
            tgs.timed_node_labels.push_back(tl);
        }

		tgs.label = graph_label;

        if (tgs.edges.size() >= min_edges && tgs.get_num_nodes() >= min_nodes) {
			tgstreams.push_back(tgs);
		} else {
        }

    }
    cout << "\nDataset name: " << datasetname << endl << flush;
    cout << "Size of dataset: " << tgstreams.size() << " graphs\n" << endl << flush;
	return datasetname;
}

void show_help() {
    cout << "Usage: ./tgraphlet <path to dataset> <mode> [sample size s]\n";
    cout << "modes:\n";
    cout << "0 \t TGK-wedge\n";
    cout << "1 \t TGK-star\n";
    cout << "2 \t TGK-all\n";
    cout << "3 \t Approx-s\n";
    exit(1);
}

int main(int argc, char *argv[]) {
    if (argc < 3) {
        show_help();
	}

	TemporalGraphStreams tgss;
	string datasetname = constructTGStreams(argv[1], tgss, 2, 1);

    unsigned int const mode = stoi(argv[2]);

    switch (mode) {
        case 0: {
            cout << "TGK-Wedge\n" << flush;
            computeTemporalGraphletKernelWedgesGramMatrix(tgss, datasetname);
            break;
        }
        case 1: {
            cout << "TGK-Star\n" << flush;
            computeTemporalGraphletKernelGramMatrix(tgss, datasetname, true, false);
            break;
        }
        case 2: {
            cout << "TGK-All\n" << flush;
            computeTemporalGraphletKernelGramMatrix(tgss, datasetname, true, true);
            break;
        }
        case 3: {
            if (argc < 4) show_help();
            unsigned int const s = stoi(argv[3]);
            cout << "Approx-" + to_string(s) +"\n" << flush;
            computeTemporalGraphletKernelApproxGramMatrix(tgss, datasetname, s, true);
            break;
        }
        default: show_help();
    }

	return 0;
}
