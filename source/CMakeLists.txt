cmake_minimum_required(VERSION 3.5)
project(tgraphlet)
set(CMAKE_CXX_STANDARD 17)

# add include directories
include_directories(
        lib/
        lib/glib-core
        lib/snap-core

        # set the environment variable EIGEN3_INCLUDE_DIR to the
        # include directory of your Eigen3 installation
        $ENV{EIGEN3_INCLUDE_DIR}

        /home/lutz/Downloads/eigen-3.3.7
)

find_package(OpenMP REQUIRED)

if (OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
endif()

add_subdirectory(src)
